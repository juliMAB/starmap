﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Assets.Scripts;
using UnityEngine;

public class Meteorits : MonoBehaviour
{
    private List<meteoritoControler> meteoritsList = new List<meteoritoControler>();
    private short cantMet = 8;
    private float timeToNextRain;

    [SerializeField]

    private List<GameObject> meteoritPrefabList;

    void Start()
    {
        timeToNextRain = 5;
    }
    void Update()
    {
        timeToNextRain -= Time.deltaTime;
        if (timeToNextRain <= 0) 
        {
            for (int i = 0; i < cantMet; i++)
            {
                GameObject go = Instantiate(meteoritPrefabList[0]);
                go.name = "meteorite " + i;
                go.transform.parent = transform;
                go.transform.position = new Vector3(Random.Range(-200, 200), Random.Range(200, 400),Random.Range(200, 400));
                go.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-20,20),0,0)-go.transform.position.normalized*90,ForceMode.Impulse);
                meteoritsList.Add(go.GetComponent<meteoritoControler>());
                if (cantMet  <= meteoritsList.Count)
                {
                    timeToNextRain = 20;
                }
            }
        }
    }
}
