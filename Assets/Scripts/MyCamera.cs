﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class MyCamera : MonoBehaviour
    {

        public float distanciaPlaneta;
        private short numPlanet;
        [SerializeField]
        private List<Transform> targeTransform;

        public GameObject go;
        private int targetSelect = 0;
        public float smoothSpeed = 0.125f;
        public Vector3 offset;

        private  void SpecialUpdate(int to)
        {
            if (to!=(int)Objs.Nave)
            {
                Vector3 desiredPosition = targeTransform[to].position + offset;
                Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
                transform.position = desiredPosition;
            }
            transform.LookAt(targeTransform[to]);
        }

        private enum Objs
        { System, Nave, Mercurio, Venus, Tierra, Marte, Júpiter, Saturno, Urano, Neptuno }

        private Objs lookAtObjs;

        private void Start()
        {
            transform.localPosition = new Vector3(75, 100, 75);
            transform.LookAt(Vector3.zero);
            for (int i = 0; i < go.GetComponent<Instancing>().generatePlanets.Count; i++)
            {
                targeTransform[i + 2] = go.GetComponent<Instancing>().generatePlanets[i].transform;
            }

            targeTransform[0] = go.transform;
        }

        private void Update()
        {
            UpdateSelected();
            Updatefollow();
        }

        private void UpdateSelected()
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                offset = new Vector3(75, 100, 75);
                targetSelect = 0;
            }
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                offset = new Vector3(0, 10, -15);
                targetSelect = 1;
                Vector3 desiredPosition = targeTransform[(int)Objs.Nave].position + offset;
                Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
                transform.position = desiredPosition;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                offset = new Vector3(0, 20, -15);
                targetSelect = 2;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                offset = new Vector3(0, 20, -15);
                targetSelect = 3;
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                offset = new Vector3(0, 20, -15);
                targetSelect = 4;
            }
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                offset = new Vector3(0, 20, -15);
                targetSelect = 5;
            }
            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                offset = new Vector3(0, 20, -15);
                targetSelect = 6;
            }
            if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                offset = new Vector3(0, 20, -15);
                targetSelect = 7;
            }
            if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                offset = new Vector3(0, 20, -15);
                targetSelect = 8;
            }
            if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                offset = new Vector3(0, 20, -15);
                targetSelect = 9;
            }
            
        }

        private void Updatefollow()
        {
            transform.parent = targeTransform[targetSelect];
            transform.LookAt(targeTransform[targetSelect]);
            SpecialUpdate(targetSelect);
        }
    }
}
