﻿
using System;
using System.Collections.Generic;
using UnityEngine;

public class TiposDeDatos : MonoBehaviour
{
    //-------------------------Tipos abstractos
    public int entero;
    public float puntoFlotante;
    public bool booleano;
    public string texto;
    public Vector3 vector;
    //-------------------------Arrays
    public int[] ArrayDeInts;
    //-------------------------List
    public List<int> intList;
    public enum Enumerador
    {
        PimerEnum,
        SegundoEnum
    }
    public Enumerador enumerado;
    //-------------------------Class
    [Serializable]
    public class ClaseBasica
    {
        public int a;
        public int b;
    }
    public ClaseBasica EjemploClase;
    //-------------------------Method
    public static void Method01() { }
}
