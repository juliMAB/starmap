﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class Instancing : MonoBehaviour
    {
        public float universalSpeedPlanets;
        public bool random;
        public Planet planetPrefab;
        public GameObject atmosphearPregabGameObject;
        public List<Planet.PlanetData> planetList;
        public List<Planet> generatePlanets = new List<Planet>();
        private readonly String[] planetsNames =
        {
            "Mercurio",
            "Venus",
            "Tierra",
            "Marte",
            "Júpiter",
            "Saturno",
            "Urano",
            "Neptuno"
        };

        private void Awake()
        {
            for (int i = 0; i < planetList.Count; i++)
            {
                Planet.PlanetData pd = planetList[i];
                GameObject go = Instantiate(planetPrefab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
                go.transform.parent = transform;
                go.GetComponent<Planet>().Init(pd);
                go.name = planetsNames[i];

                if (random)
                {
                    go.GetComponent<Planet>().Rand();
                    go.GetComponent<Planet>().data.distanceToSun += (i + 1) * 50;
                    go.GetComponent<MeshRenderer>().material = pd.mat;
                }
                if (planetsNames[i] == "Tierra")
                {
                    GameObject atm = Instantiate(atmosphearPregabGameObject, go.GetComponent<Planet>().transform.localPosition, Quaternion.identity).gameObject;
                    atm.transform.parent = go.transform;
                    atm.transform.localScale = go.transform.lossyScale + Vector3.one + Vector3.one/2;
                   
                }
                generatePlanets.Add(go.GetComponent<Planet>());
            }
        }
    }
}
