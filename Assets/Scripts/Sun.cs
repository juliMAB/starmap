﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class Sun : MonoBehaviour
    {
        [Serializable]
        public class SunData
        {
            [NonSerialized]
            public float angle;
            public Vector3 rotationDirection;
            public float traslationSpeed;
        }
        public SunData data;
        private void Start()
        {

        }

        private void Update()
        {

            data.angle += data.traslationSpeed * Time.deltaTime;
            transform.Rotate(data.rotationDirection * Time.deltaTime);
            GetComponent<Light>().intensity = Mathf.Sin(data.angle);
        }
    }
}