﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class Ship : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 50f;
    [SerializeField]
    private float turnSpeed = 60f;
    //ingresa un objeto para que se cree en la posicion de la nave.
    [SerializeField]
    GameObject shotO;

    private Transform myT;

    void Awake()
    {
        myT = transform;
    }
    void Update()
    {

        Move1();

        Shot();
    }

    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Planet>())
        {
            float dis = Vector3.Distance(other.GetComponent<Planet>().transform.position,transform.position);
            float max = GetComponent<SphereCollider>().radius*transform.localScale.x+other.GetComponent<SphereCollider>().radius*other.transform.localScale.x;
            float min = 0.1f;
            other.GetComponent<Renderer>().material.color = new Color(1, 1, 1, (dis/max)-min);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Planet>())
        {
            other.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
        }
    }

    void Move1()
    {
        turn();
        thrust();
    }

    void turn()
    {
        float hor = turnSpeed*Time.deltaTime * Input.GetAxis("Horizontal");
        float pitch = turnSpeed * Time.deltaTime * Input.GetAxis("UpDown");
        float roll = turnSpeed * Time.deltaTime * Input.GetAxis("zx");
        myT.Rotate(pitch, hor, roll);
    }

    void thrust()
    {
        if (Input.GetAxis("Vertical")>0)
        {
            myT.position += transform.forward * movementSpeed * Time.deltaTime * Input.GetAxis("Vertical");
        }
    }
    void Shot()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject go = Instantiate(shotO, transform.position, Quaternion.identity);
            go.transform.parent = transform;
            go.GetComponent<Rigidbody>().AddForce(Vector3.forward*10, ForceMode.Impulse);
            go.transform.SetParent(null);
        }
    }
}
