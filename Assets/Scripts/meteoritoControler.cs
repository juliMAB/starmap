﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class meteoritoControler : MonoBehaviour
{

    private float timeExistence;
    void Start()
    {
        timeExistence = 10;
        ParticleSystem.SetActive(false);
    }
    [SerializeField] private GameObject ParticleSystem;
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<Planet>())
        {
            ParticleSystem.SetActive(true);
            transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
            transform.GetComponent<Collider>().enabled = false;
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            Destroy(this.gameObject,5);
            other.gameObject.transform.localScale -= other.gameObject.transform.localScale/100;
        }
    }

    void Update()
    {
        timeExistence -= Time.deltaTime;
        if (timeExistence <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
