﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class Planet : MonoBehaviour
    {
        //public static float universalRotationSpeed;
        [Serializable]
        public class PlanetData
        {
           
            public float distanceToSun;
            public float traslationSpeed;
            public float size;
            [NonSerialized]
            public float angle;
            public Vector3 rotationDirection;
            public Material mat;
        }
        public PlanetData data;

        private void Start()
        {
            transform.localScale = data.size * Vector3.one;

        }

        private void Update()
        {
            updateMove();
        }

        public void updateMove()
        {
            Vector3 v3 = Vector3.zero;
            data.angle += data.traslationSpeed * Time.deltaTime;
            v3.x = data.distanceToSun * Mathf.Cos(data.angle);
            v3.z = data.distanceToSun * Mathf.Sin(data.angle);
            transform.position = v3;

            transform.Rotate(data.rotationDirection * Time.deltaTime);
        }
        public void Init(PlanetData pd)
        {
            data.distanceToSun = pd.distanceToSun;
            data.traslationSpeed = pd.traslationSpeed;
            data.rotationDirection = pd.rotationDirection;
            transform.localScale = pd.size * Vector3.one;
            data.size = pd.size;
            GetComponent<MeshRenderer>().material = pd.mat;
        }

        public void Rand()
        {
            data.distanceToSun = Random.Range(20, 50) + 50;
            data.traslationSpeed = Random.Range(0.1f, 2);
            data.rotationDirection = new Vector3(0, Random.Range(10, 50), 0);
            data.size = Random.Range(1, 30);
        }
    }
}
